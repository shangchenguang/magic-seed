package com.magic.swagger;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Lists;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * ClassName: Swagger2 <br/>
 * Function: swagger. <br/>
 * date: 2018年8月29日 下午12:43:44 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Configuration
@EnableSwagger2
public class Swagger2 {

	@Bean
	public Docket createRestApi() {// 创建API基本信息
		ParameterBuilder ticketPar = new ParameterBuilder();
		List<Parameter> pars = Lists.newArrayList();
		ticketPar.name("Authorization").description("user token").modelRef(new ModelRef("string"))
				.parameterType("header").required(false).build();
		pars.add(ticketPar.build());
		return new Docket(DocumentationType.SWAGGER_2).globalOperationParameters(pars).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("com.magic.controller"))// 扫描该包下的所有需要在Swagger中展示的API，@ApiIgnore注解标注的除外
				.paths(PathSelectors.any()).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("swagger2").description("RESTful APIs").contact("fun").version("1.0.0")
				.build();
	}

}
