package com.magic.vo.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class FileVo {

	@ApiModelProperty(name = "key", dataType = "string")
	public String key;

	@ApiModelProperty(name = "hash", dataType = "string")
	public String hash;

	@ApiModelProperty(name = "bucket", dataType = "string")
	public String bucket;

	@ApiModelProperty(name = "fsize", dataType = "Long")
	public long fsize;

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @param hash
	 *            the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}

	/**
	 * @return the bucket
	 */
	public String getBucket() {
		return bucket;
	}

	/**
	 * @param bucket
	 *            the bucket to set
	 */
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	/**
	 * @return the fsize
	 */
	public long getFsize() {
		return fsize;
	}

	/**
	 * @param fsize
	 *            the fsize to set
	 */
	public void setFsize(long fsize) {
		this.fsize = fsize;
	}

}
