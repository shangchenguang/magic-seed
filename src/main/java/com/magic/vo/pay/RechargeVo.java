package com.magic.vo.pay;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: RechargeVo <br/>
 * 充值 date: 2018年9月13日 上午10:45:28 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
public class RechargeVo {

	/**
	 * 充值金额
	 */
	@ApiModelProperty(value = "充值金额", dataType = "int", required = true, notes = "单位(分)", example = "2.5元等于250分")
	@NotNull(message = "充值金额不为空")
	private int price;

	/**
	 * 用户id
	 */
	@ApiModelProperty(value = "用户id", dataType = "int", required = true)
	@NotNull(message = "用户id不为空")
	private int userId;

	/**
	 * 支付方式
	 */
	@ApiModelProperty(value = "支付方式", dataType = "String", required = true, notes = "参数值(WESM:微信小程序支付；WECHAT：微信支付)")
	@NotBlank(message = "支付方式不为空")
	@Pattern(regexp = "^(WESM | WECHAT)$", message = "非法支付方式")
	private String payType;

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the payType
	 */
	public String getPayType() {
		return payType;
	}

	/**
	 * @param payType
	 *            the payType to set
	 */
	public void setPayType(String payType) {
		this.payType = payType;
	}

}
