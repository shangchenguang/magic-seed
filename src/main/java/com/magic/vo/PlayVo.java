package com.magic.vo;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: PlayVo <br/>
 * Function: 播种动作请求体. <br/>
 * date: 2018年10月25日 下午3:09:16 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
public class PlayVo {

	@ApiModelProperty(name = "userId", value = "用户id", dataType = "int", required = true)
	private int userId;
	
	@ApiModelProperty(name = "gameId", value = "游戏gameid", dataType = "int", required = true)
	private int gameId;
	
	@ApiModelProperty(name = "inputSeed", value = "投入花种", dataType = "bigdecimal", required = true)
	private BigDecimal inputSeed;
	
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the inputSeed
	 */
	public BigDecimal getInputSeed() {
		return inputSeed;
	}

	/**
	 * @param inputSeed the inputSeed to set
	 */
	public void setInputSeed(BigDecimal inputSeed) {
		this.inputSeed = inputSeed;
	}
    
}
