package com.magic.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: LoginCode <br/>
 * Function: 验证码快捷登陆. <br/>
 * date: 2018年10月16日 下午12:31:03 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
public class LoginCode {

	@ApiModelProperty(name = "mobile", value = "手机号", dataType = "string", required = true)
	@NotBlank(message = "mobile不为空")
	@Pattern(regexp = "^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[89]))\\d{8})$", message = "非法手机号")
	private String mobile;

	@ApiModelProperty(name = "code", value = "验证码", dataType = "string", required = true)
	@NotBlank(message = "code不为空")
	private String code;

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
