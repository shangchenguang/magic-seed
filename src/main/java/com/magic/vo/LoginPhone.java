package com.magic.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: LoginpHONE <br/>
 * Function: 手机登陆VO. <br/>
 * date: 2018年10月16日 下午12:29:15 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
public class LoginPhone {

	@ApiModelProperty(name = "mobile", value = "手机号", dataType = "string", required = true)
	@NotBlank(message = "mobile不为空")
	@Pattern(regexp = "^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[89]))\\d{8})$", message = "非法手机号")
	private String mobile;

	@ApiModelProperty(name = "password", value = "密码", dataType = "string", required = true)
	@NotBlank(message = "password不为空")
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[\\s\\S]{8,16}$",message = "密码至少包含一个大写字母,一个小写字母,一个数字,8到16位")
	private String password;

	@ApiModelProperty(name = "deviceCode", value = "设备编码", dataType = "string", required = true)
	@NotBlank(message = "deviceCode不为空")
	private String deviceCode;

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the deviceCode
	 */
	public String getDeviceCode() {
		return deviceCode;
	}

	/**
	 * @param deviceCode
	 *            the deviceCode to set
	 */
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

}
