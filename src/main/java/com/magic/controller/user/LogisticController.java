package com.magic.controller.user;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magic.result.Result;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/** 
 * ClassName: LogisticController <br/> 
 * Function: TODO ADD FUNCTION. <br/> 
 * Reason: TODO ADD REASON(可选). <br/> 
 * date: 2018年10月16日 上午9:11:21 <br/> 
 * 
 * @author lishuai11 
 * @version  
 * @since JDK 1.8
 */
@Api(value = "物流信息")
@RestController
@RequestMapping("/user/logic/")
public class LogisticController {
	@ApiOperation(value = "测试")
	public Result test() {
		return null;
	}
}
