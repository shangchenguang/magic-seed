package com.magic.controller.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magic.auth.TokenFilter;
import com.magic.result.Result;
import com.magic.service.recharge.PayService;
import com.magic.util.ValidUtil;
import com.magic.vo.pay.RechargeVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * ClassName: PayController <br/>
 * Function: 充值，礼品兑换. <br/>
 * date: 2018年10月16日 上午9:10:33 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Api(value = "充值管理")
@RestController
@RequestMapping("/user/pay/")
public class PayController {

	@Autowired
	PayService payService;

	/**
	 * 
	 * recharge:(充值). <br/>
	 * 
	 * @author lishuai11
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "充值", httpMethod = "POST", notes = "充值花种(1元等于1花种)")
	@ApiImplicitParam(name = "rechargeVo", value = "充值请求体", required = true, dataType = "RechargeVo")
	@RequestMapping(value = "recharge", method = RequestMethod.POST)
	public Result recharge(@RequestBody RechargeVo rechargeVo, HttpServletRequest request) {
		String token = request.getHeader(TokenFilter.AUTH_KEY).trim();
		List<String> list = ValidUtil.validate(rechargeVo);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return payService.recharge(rechargeVo, token);
	}

	/**
	 * 
	 * stop:(关闭交易). <br/>
	 * 
	 * @author lishuai11
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "停止支付", httpMethod = "PUT", notes = "充值花种时停止支付")
	@ApiImplicitParam(name = "tradeCode", value = "充值单编号", required = true, dataType = "string", paramType = "path")
	@RequestMapping(value = "closing/{tradeCode}", method = RequestMethod.PUT)
	public Result recharge(@PathVariable(name = "tradeCode", required = true) String tradeCode,
			HttpServletRequest request) {
		String token = request.getHeader(TokenFilter.AUTH_KEY).trim();
		return payService.stopTrade(tradeCode, token);
	}

	/**
	 * 
	 * rechargeResult:(充值结果回调). <br/>
	 * 
	 * @author lishuai11
	 * @param userId
	 * @param tradeId
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "充值结果回调", httpMethod = "GET", notes = "用于前端循环请求获取充值结果")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int", paramType = "path"),
			@ApiImplicitParam(name = "tradeCode", value = "充值单编号", required = true, dataType = "string", paramType = "path") })
	@RequestMapping(value = "recharge/{userId}/callback/{tradeCode}", method = RequestMethod.GET)
	public Result rechargeResult(@PathVariable(name = "userId") int userId,
			@PathVariable(name = "tradeCode") String tradeCode, HttpServletRequest request) {
		String token = request.getHeader(TokenFilter.AUTH_KEY).trim();
		return payService.rechargeResult(userId, tradeCode, token);
	}

	/**
	 * 
	 * rechargeForm:(交易单). <br/>
	 * 
	 * @author lishuai11
	 * @param userId
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "获取充值单", httpMethod = "GET", notes = "获取用户的充值记录")
	@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int", paramType = "path")
	@RequestMapping(value = "recharge/form/{userId}", method = RequestMethod.GET)
	public Result rechargeForm(@PathVariable(name = "userId") int userId, HttpServletRequest request) {
		String token = request.getHeader(TokenFilter.AUTH_KEY).trim();
		return payService.rechargeForm(userId, token);
	}

	/**
	 * 
	 * callback:(微信回调结果). <br/>
	 * 
	 * @author lishuai11
	 * @param request
	 * @param response
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "微信第三方回调", httpMethod = "GET", hidden = true)
	@RequestMapping(value = "wechat/callback", method = RequestMethod.GET)
	public void callback(HttpServletRequest request, HttpServletResponse response) {
		payService.callback(request, response);
	}
}
