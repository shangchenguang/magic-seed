package com.magic.controller.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magic.auth.TokenFilter;
import com.magic.entity.user.UserGame;
import com.magic.result.Result;
import com.magic.service.seed.PlayService;
import com.magic.util.ValidUtil;
import com.magic.vo.PlayVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * ClassName: PlayController <br/>
 * date: 2018年10月21日 下午4:11:48 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Api(value = "开始播种")
@RestController
@RequestMapping("/user/play/")
public class PlayController {

	@Autowired
	PlayService playService;

	@ApiOperation(value = "播种动作", httpMethod = "POST", notes = "播种", response = UserGame.class)
	@ApiImplicitParam(name = "playVo", value = "请求体", required = true, dataType = "PlayVo")
	@RequestMapping(value = "seed", method = RequestMethod.POST)
	public Result seed(@RequestBody PlayVo playVo, HttpServletRequest request) {
		String token = request.getHeader(TokenFilter.AUTH_KEY).trim();
		List<String> list = ValidUtil.validate(playVo);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return playService.seed(playVo, token);
	}

}
