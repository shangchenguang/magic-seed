package com.magic.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName: DateUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018年10月15日 下午1:40:56 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class DateUtil {
	
	/**
	 * 
	 * localTimeBySms:(短信发送获取当前时间yyyymmddhhmmss). <br/>
	 * 
	 * @author lishuai8
	 * @return
	 * @since JDK 1.8
	 */
	public static String localTimeBySms() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		return simpleDateFormat.format(new Date());
	}

	/**
	 * 
	 * long2String:(ms时间戳转化为yyyy-MM-dd HH:mm:ss). <br/>
	 * 
	 * @author lishuai11
	 * @param time
	 * @return
	 * @since JDK 1.8
	 */
	public static String long2String(long time) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return simpleDateFormat.format(new Date(time));
	}

}
