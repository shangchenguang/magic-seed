/**
 * Company:	lenovo (www.lenovo.com)
 * Project Name:fun-user-manage 
 * File Name:RandomCode.java 
 * Package Name:com.fun.util 
 * Date:2018年9月19日下午4:43:44 
 * Copyright (C) 2016,LENOVO. All rights reserved.
 * 
 */
package com.magic.util;

import java.util.UUID;

/**
 * ClassName: RandomCode <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018年9月19日 下午4:43:44 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class RandomCode {

	public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
			"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8",
			"9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
			"U", "V", "W", "X", "Y", "Z" };

	public static String generateShortUuid() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		for (int i = 0; i < 8; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			shortBuffer.append(chars[x % 0x3E]);
		}
		return shortBuffer.toString();
	}

	public static void main(String[] args) {
		System.out.println(RandomCode.generateShortUuid());
	}

}
