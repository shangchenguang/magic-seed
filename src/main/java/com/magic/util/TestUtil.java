/**
 * Company:	lenovo (www.lenovo.com)
 * Project Name:magic-seed 
 * File Name:TestUtil.java 
 * Package Name:com.magic.util 
 * Date:2018年10月29日下午3:34:44 
 * Copyright (C) 2016,LENOVO. All rights reserved.
 * 
 */
package com.magic.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName: TestUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2018年10月29日 下午3:34:44 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class TestUtil {

	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss.SSS");
	
	public static void go(String message) {
		try {

			File file = new File("/data/fun/log/test.log");
			FileOutputStream fos = new FileOutputStream(file, true);
			message = simpleDateFormat.format(new Date()) + "：" + message + "\r\n";
			if (!file.exists()) {
				file.createNewFile();
			}
			fos.write(message.getBytes());
			if (fos != null) {
				fos.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
