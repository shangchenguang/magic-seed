package com.magic.consts;

/**
 * ClassName: SystemConsts <br/>
 * Function: 系统常量. <br/>
 * date: 2018年10月15日 下午4:01:47 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class SystemConsts {

	public static final String USERNAME_PREFIX = "ms_";

	/**
	 * 微信登陆
	 */
	public static final String LOGIN_WECHAT = "WECHAT";

	/**
	 * 手机登陆
	 */
	public static final String LOGIN_PHONE = "PHONE";

	/**
	 * 性别：男
	 */
	public static final int GENDER_MAN = 1;

	/**
	 * 性别：女
	 */
	public static final int GENDER_WOMAN = 2;

	/**
	 * 性别：未知
	 */
	public static final int GENDER_UNKNOW = 0;

}
