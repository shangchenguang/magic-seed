package com.magic.consts;

/**
 * ClassName: ExchangeEnum <br/>
 * date: 2018年10月21日 下午2:16:54 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public enum ExchangeEnum {

	TOSEND("send", "待发货"), TORECEIVE("receive", "待收货"), TOEND("end", "已完成");

	private String value;

	private String name;

	ExchangeEnum(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
