/**
 * Company:	F-space
 * Project Name:fun-user-manage 
 * File Name:SystemEnum.java 
 * Package Name:com.fun 
 * Date:2018年8月20日下午6:14:20 
 * Copyright (C) 2016,F-space. All rights reserved.
 * 
 */
package com.magic.consts;

/**
 * ClassName: WechatPayEnum <br/>
 * Function: 常量. <br/>
 * date: 2018年8月20日 下午6:14:20 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public enum PayEnum {

	WECHAT("WECHAT", "微三方支付"), WESM("WESM", "微信小程序"), ALIPAY("ALIPAY", "支付宝");

	private String value;

	private String name;

	PayEnum(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
