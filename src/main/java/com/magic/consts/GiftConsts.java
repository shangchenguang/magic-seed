/**
 * Company:	lenovo (www.lenovo.com)
 * Project Name:magic-seed 
 * File Name:GiftConsts.java 
 * Package Name:com.magic.consts 
 * Date:2018年10月15日下午6:16:56 
 * Copyright (C) 2016,LENOVO. All rights reserved.
 * 
 */
package com.magic.consts;

/**
 * ClassName: GiftConsts <br/>
 * Function: 礼品类型. <br/>
 * date: 2018年10月15日 下午6:16:56 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class GiftConsts {

	/**
	 * 电话卡
	 */
	public static final String LEVEL1_PHONE_CARD = "PHONE_CARD";

	/**
	 * 中国移动
	 */
	public static final String LEVEL2_PHONE_CARD_CM = "CM";

	/**
	 * 中国电信
	 */
	public static final String LEVEL2_PHONE_CARD_CT = "CT";

	/**
	 * 中国联通
	 */
	public static final String LEVEL2_PHONE_CARD_CU = "CU";

}
