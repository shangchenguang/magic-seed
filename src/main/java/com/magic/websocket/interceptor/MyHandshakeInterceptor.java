package com.magic.websocket.interceptor;

import java.util.Map;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

/**
 * ClassName: MyHandshakeInterceptor <br/>
 * Function: MyHandshakeInterceptor. <br/>
 * date: 2018年11月15日 下午1:47:34 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
//@Component
public class MyHandshakeInterceptor implements HandshakeInterceptor {

	@Override
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Map<String, Object> attributes) throws Exception {
		String gameId = ((ServletServerHttpRequest) request).getServletRequest().getParameter("gameId");
		if (gameId == null || gameId.equals("")) {
			return false;
		} else {
			attributes.put("gameId", gameId);
			return true;
		}
	}

	@Override
	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Exception exception) {

	}

}
