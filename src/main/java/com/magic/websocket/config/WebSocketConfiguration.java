package com.magic.websocket.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.magic.websocket.handle.DefaultWebSocketHander;

@Configuration
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketConfigurer {

	@Autowired
	private DefaultWebSocketHander defaultWebSocketHander;

	// @Autowired
	// private MyHandshakeInterceptor myHandshakeInterceptor;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
		// webSocketHandlerRegistry.addHandler(defaultWebSocketHander,
		// "/magic/seed").setAllowedOrigins("*")
		// .addInterceptors(myHandshakeInterceptor);
		webSocketHandlerRegistry.addHandler(defaultWebSocketHander, "/magic/seed").setAllowedOrigins("*");
	}
}