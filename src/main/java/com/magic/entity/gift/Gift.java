/**
 * Company:	lenovo (www.lenovo.com)
 * Project Name:magic-seed 
 * File Name:Gift.java 
 * Package Name:com.magic.entity.gift 
 * Date:2018年10月15日下午5:44:54 
 * Copyright (C) 2016,LENOVO. All rights reserved.
 * 
 */
package com.magic.entity.gift;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.magic.entity.object.EntityObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: Gift <br/>
 * Function: 换购礼物. <br/>
 * date: 2018年10月15日 下午5:44:54 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
@Entity
@Table(name = "`GIFT`")
@EntityListeners(AuditingEntityListener.class)
public class Gift extends EntityObject {

	private static final long serialVersionUID = 2439897346018584832L;

	public Gift() {
		super();
	}

	public Gift(String giftName, String giftImage, String giftDec, String giftDetail, BigDecimal giftPrice,
			String oneSign, String twoSign) {
		super();
		this.giftName = giftName;
		this.giftImage = giftImage;
		this.giftDec = giftDec;
		this.giftDetail = giftDetail;
		this.giftPrice = giftPrice;
		this.oneSign = oneSign;
		this.twoSign = twoSign;
	}
	
	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`GIFT_ID`")
	private int giftId;

	/**
	 * @return the giftId
	 */
	public int getGiftId() {
		return giftId;
	}

	/**
	 * @param giftId the giftId to set
	 */
	public void setGiftId(int giftId) {
		this.giftId = giftId;
	}

	@ApiModelProperty(name = "giftName",value = "礼物名称", dataType = "String", required = true)
	@NotBlank(message = "礼物名称不可为空")
	@Column(name = "`GIFT_NAME`", nullable = false)
	private String giftName;

	@ApiModelProperty(value = "礼物图片", dataType = "String")
	@Column(name = "`GIFTIMAGE`")
	private String giftImage;

	@ApiModelProperty(name = "giftDec",value = "礼物简介", dataType = "String", required = true)
	@NotBlank(message = "礼物简介不可为空")
	@Column(name = "`GIFT_DEC`", nullable = false)
	private String giftDec;

	@ApiModelProperty(value = "礼物详细介绍", dataType = "String")
	@Column(name = "`GIFT_DETAIL`")
	private String giftDetail;

	@ApiModelProperty(name = "giftPrice",value = "礼物价格", dataType = "double", notes = "默认为0.00花种")
	@NotNull(message = "礼物价格不可为空")
	@Column(name = "`GIFT_PRICE`", nullable = false, precision = 12, scale = 2)
	private BigDecimal giftPrice = new BigDecimal(0.00);

	@ApiModelProperty(name = "oneSign",value = "礼物一级标签", dataType = "String", notes = "标签：电话卡，水果，办公文具，数码产品等等(暂时填写:PHONE_CARD,代表电话卡)")
	@Column(name = "`ONE_SIGN`")
	private String oneSign;

	@ApiModelProperty(name = "twoSign",value = "礼物二级标签", dataType = "String", notes = "标签：中国移动(CM),中国电信(CT),中国联通(CU)")
	@Column(name = "`TWO_SIGN`")
	private String twoSign;

	/**
	 * @return the giftName
	 */
	public String getGiftName() {
		return giftName;
	}

	/**
	 * @param giftName
	 *            the giftName to set
	 */
	public void setGiftName(String giftName) {
		this.giftName = giftName;
	}

	/**
	 * @return the giftImage
	 */
	public String getGiftImage() {
		return giftImage;
	}

	/**
	 * @param giftImage
	 *            the giftImage to set
	 */
	public void setGiftImage(String giftImage) {
		this.giftImage = giftImage;
	}

	/**
	 * @return the giftDec
	 */
	public String getGiftDec() {
		return giftDec;
	}

	/**
	 * @param giftDec
	 *            the giftDec to set
	 */
	public void setGiftDec(String giftDec) {
		this.giftDec = giftDec;
	}

	/**
	 * @return the giftDetail
	 */
	public String getGiftDetail() {
		return giftDetail;
	}

	/**
	 * @param giftDetail
	 *            the giftDetail to set
	 */
	public void setGiftDetail(String giftDetail) {
		this.giftDetail = giftDetail;
	}

	/**
	 * @return the giftPrice
	 */
	public BigDecimal getGiftPrice() {
		return giftPrice;
	}

	/**
	 * @param giftPrice
	 *            the giftPrice to set
	 */
	public void setGiftPrice(BigDecimal giftPrice) {
		this.giftPrice = giftPrice;
	}

	/**
	 * @return the oneSign
	 */
	public String getOneSign() {
		return oneSign;
	}

	/**
	 * @param oneSign
	 *            the oneSign to set
	 */
	public void setOneSign(String oneSign) {
		this.oneSign = oneSign;
	}

	/**
	 * @return the twoSign
	 */
	public String getTwoSign() {
		return twoSign;
	}

	/**
	 * @param twoSign
	 *            the twoSign to set
	 */
	public void setTwoSign(String twoSign) {
		this.twoSign = twoSign;
	}

}
