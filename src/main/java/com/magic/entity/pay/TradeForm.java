package com.magic.entity.pay;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.magic.consts.TradeEnum;
import com.magic.entity.object.EntityObject;

/**
 * ClassName: Trade <br/>
 * Function: 交易单. <br/>
 * date: 2018年9月15日 上午11:23:50 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Entity
@Table(name = "`TRADE_FORM`")
@EntityListeners(AuditingEntityListener.class)
public class TradeForm extends EntityObject {

	private static final long serialVersionUID = -5159421108880205659L;

	public TradeForm() {
		super();
	}

	public TradeForm(int userId, String tradeDetail, int money) {
		this.userId = userId;
		this.tradeDetail = tradeDetail;
		this.money = money;
		this.tradeStatus = TradeEnum.STATUS_ING.getValue();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`TRADEFORM_ID`")
	private int tradeFormId;

	/**
	 * @return the tradeFormId
	 */
	public int getTradeFormId() {
		return tradeFormId;
	}

	/**
	 * @param tradeFormId
	 *            the tradeFormId to set
	 */
	public void setTradeFormId(int tradeFormId) {
		this.tradeFormId = tradeFormId;
	}

	@Column(name = "`USER_ID`", nullable = false, updatable = false)
	private int userId;

	@Column(name = "`TRADE_CODE`", unique = true)
	private String tradeCode;

	@Column(name = "`TRADE_DETAIL`")
	private String tradeDetail;

	@Column(name = "`MONEY`")
	private int money;

	@Column(name = "`TRADE_STATUS`")
	private String tradeStatus;

	@Column(name = "`FAIL_REASON`")
	private String failReason;

	@Column(name = "`TRADE_TIME`")
	private String tradeTime;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the tradeId
	 */
	public String getTradeCode() {
		return tradeCode;
	}

	/**
	 * @param tradeId
	 *            the tradeId to set
	 */
	public void setTradeCode(String tradeCode) {
		this.tradeCode = tradeCode;
	}

	/**
	 * @return the tradeDetail
	 */
	public String getTradeDetail() {
		return tradeDetail;
	}

	/**
	 * @param tradeDetail
	 *            the tradeDetail to set
	 */
	public void setTradeDetail(String tradeDetail) {
		this.tradeDetail = tradeDetail;
	}

	/**
	 * @return the money
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * @param money
	 *            the money to set
	 */
	public void setMoney(int money) {
		this.money = money;
	}

	/**
	 * @return the tradeStatus
	 */
	public String getTradeStatus() {
		return tradeStatus;
	}

	/**
	 * @param tradeStatus
	 *            the tradeStatus to set
	 */
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	/**
	 * @return the failReason
	 */
	public String getFailReason() {
		return failReason;
	}

	/**
	 * @param failReason
	 *            the failReason to set
	 */
	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	/**
	 * @return the tradeTime
	 */
	public String getTradeTime() {
		return tradeTime;
	}

	/**
	 * @param tradeTime
	 *            the tradeTime to set
	 */
	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

}
