package com.magic.entity.user;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.magic.entity.object.EntityObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: Flower <br/>
 * Function: 花种. <br/>
 * date: 2018年10月15日 下午4:56:46 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
@Entity
@Table(name = "`FLOWER`")
@EntityListeners(AuditingEntityListener.class)
public class Flower extends EntityObject {

	private static final long serialVersionUID = -3040840092471981129L;

	public Flower() {
		super();
	}

	public Flower(int userId) {
		super();
		this.userId = userId;
	}

	@ApiModelProperty(name = "flowerId", value = "花种id", dataType = "int")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`FLOWER_ID`")
	private int flowerId;

	@ApiModelProperty(name = "userId", value = "用户id", dataType = "int")
	@Column(name = "`USER_ID`", unique = true, nullable = false, updatable = false)
	private int userId;

	@ApiModelProperty(name = "rechargeSeed", value = "账户花种(两位小数)", dataType = "double")
	@Column(name = "`RECHARGE_SEED`", precision = 12, scale = 2, nullable = false)
	private BigDecimal rechargeSeed = new BigDecimal(0.00);

	@ApiModelProperty(name = "allSeed", value = "总花种(两位小数)", dataType = "double")
	@Column(name = "`ALL_SEED`", precision = 12, scale = 2, nullable = false)
	private BigDecimal allSeed = new BigDecimal(0.00);

	@ApiModelProperty(name = "earnSeed", value = "收益花种(两位小数)", dataType = "double")
	@Column(name = "`EARN_SEED`", precision = 12, scale = 2, nullable = false)
	private BigDecimal earnSeed = new BigDecimal(0.00);

	/**
	 * @return the flowerId
	 */
	public int getFlowerId() {
		return flowerId;
	}

	/**
	 * @param flowerId
	 *            the flowerId to set
	 */
	public void setFlowerId(int flowerId) {
		this.flowerId = flowerId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the rechargeSeed
	 */
	public BigDecimal getRechargeSeed() {
		return rechargeSeed;
	}

	/**
	 * @param rechargeSeed
	 *            the rechargeSeed to set
	 */
	public void setRechargeSeed(BigDecimal rechargeSeed) {
		this.rechargeSeed = rechargeSeed;
	}

	/**
	 * @return the allSeed
	 */
	public BigDecimal getAllSeed() {
		return allSeed;
	}

	/**
	 * @param allSeed
	 *            the allSeed to set
	 */
	public void setAllSeed(BigDecimal allSeed) {
		this.allSeed = allSeed;
	}

	/**
	 * @return the earnSeed
	 */
	public BigDecimal getEarnSeed() {
		return earnSeed;
	}

	/**
	 * @param earnSeed
	 *            the earnSeed to set
	 */
	public void setEarnSeed(BigDecimal earnSeed) {
		this.earnSeed = earnSeed;
	}

}
