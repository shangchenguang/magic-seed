/**
 * Company:	lenovo (www.lenovo.com)
 * Project Name:magic-seed 
 * File Name:Address.java 
 * Package Name:com.magic.entity.user 
 * Date:2018年10月15日下午4:19:31 
 * Copyright (C) 2016,LENOVO. All rights reserved.
 * 
 */
package com.magic.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.magic.entity.object.EntityObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: Address <br/>
 * Function: 收货地址. <br/>
 * date: 2018年10月15日 下午4:19:31 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel()
@Entity
@Table(name = "`ADDRESS`")
@EntityListeners(AuditingEntityListener.class)
public class Address extends EntityObject {

	private static final long serialVersionUID = 5022999075759401519L;

	public Address() {
		super();
	}

	public Address(int userId, String mobile, String receiveName, String province, String city, String unit,
			String detail, String code, boolean select) {
		super();
		this.userId = userId;
		this.mobile = mobile;
		this.receiveName = receiveName;
		this.province = province;
		this.city = city;
		this.unit = unit;
		this.detail = detail;
		this.code = code;
		this.select = select;
	}

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`ADDRESS_ID`")
	private int addressId;

	@ApiModelProperty(value = "用户id", dataType = "int", required = true, notes = "用户id")
	@NotNull(message = "用户id不能为空")
	@Column(name = "`USER_ID`", nullable = false, updatable = false)
	private int userId;

	@ApiModelProperty(name = "mobile", value = "收货人电话", dataType = "String", required = true)
	@NotBlank(message = "收货电话不为空")
	@Pattern(regexp = "((^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[89]))\\d{8})$))|(^(0\\d{2}-\\d{8}(-\\d{1,4})?)|(0\\d{3}-\\d{7,8}(-\\d{1,4})?)$)", message = "非法电话号码")
	@Column(name = "`MOBILE`", nullable = false)
	private String mobile;

	@ApiModelProperty(name = "receiveName", value = "收件人", dataType = "String", required = true)
	@NotBlank(message = "收件人不可为空")
	@Column(name = "`RECEIVENAME`", nullable = false)
	private String receiveName;

	@ApiModelProperty(name = "province", value = "省份", dataType = "String", required = true)
	@NotBlank(message = "省份不为空")
	@Column(name = "`PROVINCE`", nullable = false)
	private String province;

	@ApiModelProperty(name = "city", value = "城市", dataType = "String", required = true)
	@NotBlank(message = "城市不为空")
	@Column(name = "`CITY`", nullable = false)
	private String city;

	@ApiModelProperty(name = "unit", value = "市区", dataType = "String", required = true)
	@NotBlank(message = "市区不为空")
	@Column(name = "`UNIT`", nullable = false)
	private String unit;

	@ApiModelProperty(name = "detail", value = "详细地址", dataType = "String", required = true)
	@NotBlank(message = "详细地址不为空")
	@Column(name = "`DETAIL`", nullable = false)
	private String detail;

	@ApiModelProperty(name = "code", value = "邮编", dataType = "String", required = true)
	@NotBlank(message = "邮编不为空")
	@Pattern(regexp = "^[0-9]{6}$", message = "非法邮编")
	@Column(name = "`CODE`", nullable = false)
	private String code;

	@ApiModelProperty(name = "select", value = "是否默认", dataType = "Boolean", required = false)
	@Column(name = "`SELECT`", nullable = false)
	private boolean select = false;

	/**
	 * @return the addressId
	 */
	public int getAddressId() {
		return addressId;
	}

	/**
	 * @param addressId
	 *            the addressId to set
	 */
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the receiveName
	 */
	public String getReceiveName() {
		return receiveName;
	}

	/**
	 * @param receiveName
	 *            the receiveName to set
	 */
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province
	 *            the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit
	 *            the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail
	 *            the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the select
	 */
	public boolean isSelect() {
		return select;
	}

	/**
	 * @param select
	 *            the select to set
	 */
	public void setSelect(boolean select) {
		this.select = select;
	}

}
