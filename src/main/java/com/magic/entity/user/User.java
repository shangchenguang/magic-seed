package com.magic.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.magic.consts.SystemConsts;
import com.magic.entity.object.EntityObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: User <br/>
 * Function: 用户表. <br/>
 * date: 2018年8月29日 下午12:37:59 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
@Entity
@Table(name = "`USER`")
@EntityListeners(AuditingEntityListener.class)
public class User extends EntityObject {

	private static final long serialVersionUID = 7446444606001941793L;

	public User() {
		super();
	}

	/**
	 * 
	 * 手机注册.
	 * 
	 * @param userName
	 * @param password
	 * @param deviceCode
	 */
	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
		this.mobile = userName;
		this.loginType = SystemConsts.LOGIN_PHONE;
		this.gender = 0;
	}

	/**
	 * 
	 * 微信登陆.
	 * 
	 * @param openId
	 */
	public User(String openId) {
		super();
		this.openId = openId;
		this.loginType = SystemConsts.LOGIN_WECHAT;
		this.gender = 0;
	}

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`USER_ID`")
	private int userId;

	@ApiModelProperty(hidden = true)
	@Column(name = "`USERNAME`", unique = true, nullable = false, updatable = false)
	private String userName;
	
	@ApiModelProperty(hidden = true)
	@Column(name = "`USERCode`", unique = true)
	private String userCode;

	@ApiModelProperty(name = "nickName", value = "昵称", dataType = "String")
	@Column(name = "`NICKNAME`")
	private String nickName;

	@ApiModelProperty(name = "icon", value = "头像", dataType = "String")
	@Column(name = "`ICON`")
	private String icon;

	@ApiModelProperty(name = "gender", value = "性别", dataType = "int", notes = "1:男；2:女；0:未知")
	@Column(name = "`GENDER`", nullable = false)
	private int gender;

	@ApiModelProperty(name = "loginType", value = "登陆方式", hidden = true)
	@Column(name = "`LOGINTYPE`", nullable = false)
	private String loginType;

	@ApiModelProperty(name = "openId", hidden = true)
	@Column(name = "`OPENID`")
	private String openId;

	@ApiModelProperty(name = "password", hidden = true)
	@Column(name = "`PASSWORD`")
	private String password;

	@ApiModelProperty(hidden = true)
	@Column(name = "`LOCKED`")
	private boolean locked = false;

	@ApiModelProperty(name = "deviceCode", hidden = true)
	@Column(name = "`DEVICECODE`")
	private String deviceCode;

	@ApiModelProperty(name = "mobile", value = "手机号", dataType = "String")
	@Pattern(regexp = "^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[89]))\\d{8})$", message = "非法手机号")
	@Column(name = "`MOBILE`")
	private String mobile;

	@ApiModelProperty(name = "city", value = "市区", dataType = "String")
	@Column(name = "`CITY`")
	private String city;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getDeviceCode() {
		return deviceCode;
	}

	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the userCode
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * @param userCode the userCode to set
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

}
