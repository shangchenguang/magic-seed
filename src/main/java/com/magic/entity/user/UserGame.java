package com.magic.entity.user;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: UserGame <br/>
 * Function: 用户游戏. <br/>
 * date: 2018年10月24日 下午4:21:54 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
@Entity
@Table(name = "`USER_GAME`")
@EntityListeners(AuditingEntityListener.class)
public class UserGame implements Serializable {

	private static final long serialVersionUID = -420959308947899738L;

	public UserGame() {

	}

	public UserGame(int userId, int gameId, String name, String detail, String image, BigDecimal userInputs) {
		this.userId = userId;
		this.gameId = gameId;
		this.name = name;
		this.detail = detail;
		this.image = image;
		this.userInputs = userInputs;
	}

	@ApiModelProperty(name = "userGameId", value = "主键", dataType = "int")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`USERGAME_ID`")
	private int userGameId;

	@ApiModelProperty(name = "userId", value = "用户id", dataType = "int")
	@Column(name = "`USER_ID`", nullable = false, updatable = false)
	private int userId;

	@ApiModelProperty(name = "gameId", value = "游戏id", dataType = "int")
	@Column(name = "`GAME_ID`", nullable = false, updatable = false)
	private int gameId;

	@ApiModelProperty(name = "name", value = "游戏名称", dataType = "string")
	@NotBlank(message = "游戏名称不为空")
	@Column(name = "`NAME`", nullable = false)
	private String name;

	@ApiModelProperty(name = "detail", value = "游戏描述", dataType = "string")
	@NotBlank(message = "游戏描述不为空")
	@Column(name = "`DETAIL`", nullable = false)
	private String detail;

	@ApiModelProperty(name = "image", value = "游戏图片", dataType = "string")
	@NotBlank(message = "游戏图片不为空")
	@Column(name = "`IMAGE`", nullable = false)
	private String image;

	@ApiModelProperty(name = "userInputs", value = "用户投入花种", dataType = "double")
	@Column(name = "`USER_INPUTS`", precision = 12, scale = 2, nullable = false)
	private BigDecimal userInputs = new BigDecimal(0.00);

	@ApiModelProperty(name = "userReceiver", value = "用户收益", dataType = "double")
	@Column(name = "`INPUT_SEEDS`", precision = 12, scale = 2, nullable = false)
	private BigDecimal userReceiver = new BigDecimal(0.00);

	@ApiModelProperty(name = "time", value = "创建时间")
	@CreatedDate
	@Column(name = "`TIME`", updatable = false, columnDefinition = "bigint(20) DEFAULT 0")
	private long time;

	@ApiModelProperty(name = "end", value = "游戏是否结束(视图字段)", dataType = "Boolean")
	@Column(name = "`END`")
	@Transient
	private Boolean end;

	@ApiModelProperty(name = "endUser", value = "最终成交人(视图字段)")
	@CreatedDate
	@Transient
	private String endUser;

	@ApiModelProperty(name = "endUserInput", value = "最终成交人花费(视图字段)")
	@CreatedDate
	@Transient
	private BigDecimal endUserInput;

	@ApiModelProperty(name = "endUserReceiver", value = "最终成交人收益(视图字段)")
	@CreatedDate
	@Transient
	private BigDecimal endUserReceiver;

	/**
	 * @return the userGameId
	 */
	public int getUserGameId() {
		return userGameId;
	}

	/**
	 * @param userGameId
	 *            the userGameId to set
	 */
	public void setUserGameId(int userGameId) {
		this.userGameId = userGameId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @param gameId
	 *            the gameId to set
	 */
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail
	 *            the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the userInputs
	 */
	public BigDecimal getUserInputs() {
		return userInputs;
	}

	/**
	 * @param userInputs
	 *            the userInputs to set
	 */
	public void setUserInputs(BigDecimal userInputs) {
		this.userInputs = userInputs;
	}

	/**
	 * @return the userReceiver
	 */
	public BigDecimal getUserReceiver() {
		return userReceiver;
	}

	/**
	 * @param userReceiver
	 *            the userReceiver to set
	 */
	public void setUserReceiver(BigDecimal userReceiver) {
		this.userReceiver = userReceiver;
	}

	/**
	 * @return the endUser
	 */
	public String getEndUser() {
		return endUser;
	}

	/**
	 * @param endUser
	 *            the endUser to set
	 */
	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	/**
	 * @return the endUserInput
	 */
	public BigDecimal getEndUserInput() {
		return endUserInput;
	}

	/**
	 * @param endUserInput
	 *            the endUserInput to set
	 */
	public void setEndUserInput(BigDecimal endUserInput) {
		this.endUserInput = endUserInput;
	}

	/**
	 * @return the endUserReceiver
	 */
	public BigDecimal getEndUserReceiver() {
		return endUserReceiver;
	}

	/**
	 * @param endUserReceiver
	 *            the endUserReceiver to set
	 */
	public void setEndUserReceiver(BigDecimal endUserReceiver) {
		this.endUserReceiver = endUserReceiver;
	}

	/**
	 * @return the end
	 */
	public Boolean getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(Boolean end) {
		this.end = end;
	}

}
