package com.magic.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.magic.auth.TokenFilter;

/**
 * ClassName: ResourceAuditAspect <br/>
 * Function: ResourceAuditAspect. <br/>
 * date: 2018年11月8日 下午4:29:28 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
//@Component
//@Aspect
public class ResourceAuditAspect {

	private static final Logger logger = LoggerFactory.getLogger(ResourceAuditAspect.class);

	@Pointcut("@annotation(ResourceAudit)")
	public void ResourceAuditAspect() {

	}

	@Before("ResourceAuditAspect()")
	public void operationAsp(JoinPoint jp) throws Throwable {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		if (attributes != null) {
			HttpServletRequest request = attributes.getRequest();
			String token = request.getHeader(TokenFilter.AUTH_KEY);
		}
	}

}
