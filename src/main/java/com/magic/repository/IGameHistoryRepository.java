package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.game.GameHistory;

/**
 * ClassName: IUserHistoryRepository <br/>
 * Function: 用户历史数据. <br/>
 * date: 2018年10月24日 下午4:15:44 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Transactional
public interface IGameHistoryRepository
		extends PagingAndSortingRepository<GameHistory, Integer>, JpaSpecificationExecutor<GameHistory> {

	@Query(nativeQuery = true, value = "SELECT g.* FROM game_history g where g.game_id = ?3 ORDER BY g.time DESC limit ?1,?2")
	List<GameHistory> pageSearchGameHistory(int pageNum, int pageSize, int gameId);

	@Query(nativeQuery = true, value = "SELECT g.* FROM game_history g GROUP BY g.user_id")
	List<GameHistory> findGroupByUserId();

	@Query("SELECT g FROM GameHistory g WHERE g.gameId = ?1 ORDER BY g.time DESC")
	List<GameHistory> findByGameId(int gameId);
}
