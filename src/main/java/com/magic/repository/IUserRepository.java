package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.user.User;

/**
 * ClassName: IUserRepository <br/>
 * date: 2018年8月29日 下午1:16:38 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Transactional
public interface IUserRepository extends PagingAndSortingRepository<User, Integer>, JpaSpecificationExecutor<User> {

	List<User> findByOpenId(String openId);
	
	List<User> findByUserName(String userName);

}
