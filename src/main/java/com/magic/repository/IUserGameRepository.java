package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.user.UserGame;

/**
 * ClassName: IUserGameRepository <br/>
 * Function: 用户历史数据. <br/>
 * date: 2018年10月24日 下午4:15:44 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Transactional
public interface IUserGameRepository
		extends PagingAndSortingRepository<UserGame, Integer>, JpaSpecificationExecutor<UserGame> {

	@Query(nativeQuery = true, value = "SELECT u.* FROM user_game u WHERE u.user_id = ?3 ORDER BY u.time DESC limit ?1,?2")
	List<UserGame> pageSearchUserGame(int pageNum, int pageSize, int userId);

	List<UserGame> findByUserIdAndGameId(int userId, int gameId);
}
