package com.magic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.game.Game;

/**
 * ClassName: IGameRepository <br/>
 * date: 2018年10月21日 下午3:54:30 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public interface IGameRepository extends PagingAndSortingRepository<Game, Integer>, JpaSpecificationExecutor<Game> {

	@Query(nativeQuery = true, value = "SELECT g.* FROM game g ORDER BY g.createdtime DESC limit ?1,?2")
	List<Game> pageSearchfindAll(int pageNum, int pageSize);

	List<Game> findByEnd(boolean end);
}
