package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.user.Address;

/**
 * ClassName: IAddressRepository <br/>
 * Function: 地址管理. <br/>
 * date: 2018年10月16日 下午3:49:20 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Transactional
public interface IAddressRepository
		extends PagingAndSortingRepository<Address, Integer>, JpaSpecificationExecutor<Address> {

	void deleteByUserId(int userId);

	@Query("SELECT a FROM Address a WHERE a.userId = ?1 ORDER BY a.createdTime DESC")
	List<Address> findByUserId(int userId);
	
	@Query("SELECT a FROM Address a WHERE a.addressId != ?1")
	List<Address> findAddressNotContainAddressId(int addressId);

}
