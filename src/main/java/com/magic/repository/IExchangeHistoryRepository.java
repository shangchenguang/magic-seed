package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.user.ExchangeHistory;

/**
 * ClassName: IExchangeHistoryRepository <br/>
 * Function: 兑换记录表. <br/>
 * date: 2018年10月21日 下午2:35:37 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Transactional
public interface IExchangeHistoryRepository
		extends PagingAndSortingRepository<ExchangeHistory, Integer>, JpaSpecificationExecutor<ExchangeHistory> {

	@Query(nativeQuery = true, value = "SELECT e.* FROM exchange_history e where e.user_id = ?3 ORDER BY e.exchange_time DESC limit ?1,?2")
	List<ExchangeHistory> pageSearchByUserId(int pageNum, int pageSize, int userId);
}
