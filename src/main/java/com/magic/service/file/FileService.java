package com.magic.service.file;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.magic.result.Result;
import com.magic.util.QiniuUtils;

@Service("fileService")
public class FileService {

	private static final Logger logger = LoggerFactory.getLogger(FileService.class);

	public Result upload(MultipartFile file) {
		try {
			InputStream inputStream = file.getInputStream();
			// String fileName = file.getOriginalFilename();
			return Result.success("上传成功", QiniuUtils.upload(inputStream));
		} catch (IOException e) {
			logger.error("FileService.upload()方法异常{}:{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

}
