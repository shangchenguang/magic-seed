package com.magic.service.gift;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magic.entity.gift.Gift;
import com.magic.repository.IGiftRepository;
import com.magic.result.Result;

/**
 * ClassName: GiftService <br/>
 * Function: 礼品中心. <br/>
 * date: 2018年10月16日 下午7:10:55 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Service("giftService")
public class GiftService {

	private static final Logger logger = LoggerFactory.getLogger(GiftService.class);

	@Autowired
	IGiftRepository giftRepository;

	/**
	 * 
	 * giftList:(礼物列表). <br/>
	 * 
	 * @author lishuai11
	 * @return
	 * @since JDK 1.8
	 */
	public Result giftList(int pageNum, int pageSize) {
		try {
			pageNum = pageNum * pageSize;
			return Result.success("成功", giftRepository.pageSearch(pageNum, pageSize));
		} catch (Exception e) {
			logger.error("GiftService.giftList()方法异常{},{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	public Result giftOne(int giftId) {
		try {
			return Result.success("成功", findOne(giftId));
		} catch (Exception e) {
			logger.error("GiftService.giftOne()方法异常{},{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	/**
	 * 
	 * deleteGift:(删除礼物). <br/>
	 * 
	 * @author lishuai11
	 * @param giftId
	 * @return
	 * @since JDK 1.8
	 */
	public Result deleteGift(int giftId) {
		try {
			giftRepository.delete(giftId);
			return Result.success("删除成功");
		} catch (Exception e) {
			logger.error("GiftService.deleteGift()方法异常{},{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	/**
	 * 
	 * addGift:(添加礼物). <br/>
	 * 
	 * @author lishuai11
	 * @param gift
	 * @since JDK 1.8
	 */
	public Result addGift(Gift gift) {
		try {
			giftRepository.save(gift);
			return Result.success("成功");
		} catch (Exception e) {
			logger.error("GiftService.addGift()方法异常{},{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	/**
	 * 
	 * updateGift:(更新礼物). <br/>
	 * 
	 * @author lishuai11
	 * @param gift
	 * @since JDK 1.8
	 */
	public Result updateGift(int giftId, Gift gift) {
		try {
			Gift dbGift = findOne(giftId);
			if (dbGift == null) {
				return Result.fail(Result.CLIENT_ERROR, "礼物不存在");
			}
			dbGift.setGiftDec(gift.getGiftDec());
			dbGift.setGiftDetail(gift.getGiftDetail());
			dbGift.setGiftImage(gift.getGiftImage());
			dbGift.setGiftName(gift.getGiftName());
			dbGift.setGiftPrice(gift.getGiftPrice());
			dbGift.setOneSign(gift.getOneSign());
			dbGift.setTwoSign(gift.getTwoSign());
			giftRepository.save(dbGift);
			return Result.success("成功");
		} catch (Exception e) {
			logger.error("GiftService.updateGift()方法异常{},{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	public Gift findOne(int giftId) {
		return giftRepository.findOne(giftId);
	}

}
